'use strict';

const spawn = require('child_process').spawn;
const express = require('express');
const CLEAR_WEATHER_INTERVAL = 60 * 1000;

// sudo java -server -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:+CMSIncrementalPacing -XX:ParallelGCThreads=7 -XX:+AggressiveOpts -Xms512M -Xmx1008M -jar /home/pi/mc/spigot-current.jar nogui
const mcProcess = spawn('java', [
	'-server',
	'-XX:+UseConcMarkSweepGC',
	'-XX:+UseParNewGC',
	'-XX:+CMSIncrementalPacing',
	'-XX:ParallelGCThreads=7',
	'-XX:+AggressiveOpts',
	'-Xms512M',
	'-Xmx1008M',
	'-jar',
	'spigot-current.jar',
	'nogui'
], {
	cwd: '/home/pi/mc'
});

/**
 * Log data coming from the process to stdout.
 */ 
const log = data => {
	process.stdout.write(data.toString());
};

// Set stdout and stderr to use our log function.
mcProcess.stdout.on('data', log);
mcProcess.stderr.on('data', log);

process.on('exit', () => {
	mcProcess.kill();
});

setInterval(() => {
	mcProcess.stdin.write('weather clear\n');
}, CLEAR_WEATHER_INTERVAL);
