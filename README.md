```
  ██████████
  █░░░░░░░░█
  █░██░░██░█
  █░░░██░░░█
  █░░████░░█
  █░░█░░█░░█
  █░░░░░░░░█
  ██████████
  ──█░░░░█
  ──█░░░░█
  ──█░░░░█
  ──█░░░░█
  ──█░░░░█
  ──█░░░░█
  ██████████
  █░░░██░░░█
  █░░░██░░░█
  █▄█▄██▄█▄█
```

A convenient wrapper for Minecraft servers.

Current functionality:
- once per minute, set weather to 'clear'.
